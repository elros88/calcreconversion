function actionMenu() {
    if(stack.depth > 1) {
        stack.pop();
    }
    else {
        navigationMenu.open();
    }
}

function abrirVista(vista) {
    stack.push(vista);
    navigationMenu.close();
}
