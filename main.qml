import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2

import "qrc:/controlador/controlador.js" as Controlador

ApplicationWindow {
    visible: true
    width: Screen.width
    height: Screen.height
    title: qsTr("Calculadora para Reconversion")

    header: ToolBar {
        contentHeight: toolbutton.implicitHeight

        ToolButton {
            id: toolbutton
            text: stack.depth > 1 ? "\u25C0" : "\u2630"
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: Controlador.actionMenu()
        }

        Label {
            text: stack.currentItem.title
            anchors.centerIn: parent
        }
    }

    Drawer {
        id: navigationMenu
        width: parent.width * 0.6
        height: parent.height

        Flickable
        {
            anchors.fill: parent
            contentWidth: width
            contentHeight: height*1.5
            Column {
                anchors.centerIn: parent
                width: parent.width*0.9
                height: parent.height*0.9

                Text
                {
                    text: qsTr("CONVERSIONES")
                    width: parent.width
                }

                ItemDelegate {
                    text: qsTr("Bolívar Fuerte a Soberano")
                    width: parent.width
                    onClicked: Controlador.abrirVista("qrc:/conversionBsfBss")
                }

                ItemDelegate {
                    text: qsTr("Bolívar Soberano a Fuerte")
                    width: parent.width
                    onClicked: Controlador.abrirVista("qrc:/conversionBssBsf")
                }

                ItemDelegate {
                    text: qsTr("Bolívar a Petro")
                    width: parent.width
                    onClicked: Controlador.abrirVista("qrc:/conversionBsPtr")
                }

                ItemDelegate {
                    text: qsTr("Petro a Bolívar")
                    width: parent.width
                    onClicked: Controlador.abrirVista("qrc:/conversionPtrBs")
                }

                ItemDelegate {
                    text: qsTr("Dólar a Petro")
                    width: parent.width
                    onClicked: Controlador.abrirVista("qrc:/conversionUsdPtr")
                }
                ItemDelegate {
                    text: qsTr("Dólar a Bolívar")
                    width: parent.width
                    onClicked: Controlador.abrirVista("qrc:/conversionUsdBs")
                }
                Text
                {
                    text: qsTr("SALARIOS")
                    width: parent.width
                }
                ItemDelegate {
                    text: qsTr("Nuevo Salario Bs.S")
                    width: parent.width
                    onClicked: Controlador.abrirVista("qrc:/salarioBsS")
                }
                ItemDelegate {
                    text: qsTr("Nuevo Salario Bs.F")
                    width: parent.width
                    onClicked: Controlador.abrirVista("qrc:/salarioBsF")
                }
                ItemDelegate {
                    text: qsTr("Salario en Dólar")
                    width: parent.width
                    onClicked: Controlador.abrirVista("qrc:/salarioUsd")
                }
                Text
                {
                    text: qsTr("MAS INFORMACIÓN")
                    width: parent.width
                }
                ItemDelegate {
                    text: qsTr("Acerca De")
                    width: parent.width
                    onClicked: Controlador.abrirVista("qrc:/acerca")
                }

            }

        }

    }

    FontLoader { id: droidSans; source: "qrc:/fonts/myCustomFont.ttf" }
    StackView {
        id: stack
        initialItem: "qrc:/home"
        anchors.fill: parent
    }
}
