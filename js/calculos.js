function conversionBolivaresPetro(bolivares)
{
    return (bolivares / constantes.bsAptr).toFixed(2);
}

function conversionPetroBolivares(petros)
{
    return (petros * constantes.bsAptr).toFixed(2);
}

function conversionFuertesSoberanos(bsf)
{
    return (bsf / constantes.reduccionCeros).toFixed(2);
}

function conversionSoberanosFuertes(bss)
{
    return (bss * constantes.reduccionCeros).toFixed(2);
}

function conversionDolarPetro(dolar)
{
    return (dolar / constantes.dolarAptr).toFixed(2);
}

function conversionDolarBolivar(dolar)
{
    return ((dolar / constantes.dolarAptr) * constantes.bsAptr).toFixed(2);
}

function calcularAumentoSoberano(salarioSoberano)
{
    return ((salarioSoberano * constantes.porcentajeAumento) / 100).toFixed(2);
}

function calcularAumentoFuerte(salarioFuerte)
{
    return ((salarioFuerte * constantes.porcentajeAumento) / 100).toFixed(2);
}

function calcularSalarioDolarPetro(salarioDolar)
{
    return (salarioDolar / constantes.dolarAptr).toFixed(2);
}

function calcularSalarioDolarBolivar(salarioDolar)
{
    return ((salarioDolar / constantes.dolarAptr) * constantes.bsAptr).toFixed(2);
}
