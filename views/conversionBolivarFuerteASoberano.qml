import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import "qrc:/utils/calculos.js" as Calculos

Page
{
    width: parent.width
    height: parent.height

    title: qsTr("Conversión Bolívar Fuerte a Soberano")

    Text
    {
        id: instrucciones;
        text: qsTr("Introduzca un valor en Bolívares Fuertes para calcular su equivalente en Bolívares Soberanos")
        width: parent.width * 0.8
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 20
        wrapMode: Text.WordWrap
    }

    ColumnLayout
    {
        anchors.top: instrucciones.bottom
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width*0.8

        Label
        {
            text: qsTr("Bolívares Fuertes:")
        }
        TextField
        {
            id: valor
            placeholderText: qsTr("Bs.F")
            inputMethodHints: Qt.ImhDigitsOnly
        }
        Button
        {
            text: qsTr("Convertir")
            onClicked:
            {
                var bsf = Calculos.conversionFuertesSoberanos(valor.text)
                resultado.text = qsTr(valor.text+"Bs.F es igual a " + bsf + "Bs.S")
                resultado.visible = true;
            }
        }
        Text
        {
            id: resultado
            visible: false
        }

    }


}
