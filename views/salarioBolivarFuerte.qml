import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import "qrc:/utils/calculos.js" as Calculos

Page
{
    width: parent.width
    height: parent.height

    title: qsTr("Salario Bs.F")

    Text
    {
        id: instrucciones;
        text: qsTr("Introduzca su salario base actual en Bolívares Fuerte y obtenga su nuevo salario después de aplicar el aumento de 3600% decretado por el Ejecutivo Nacional \n
nota: el valor es referencia y no refleja realmente en cuanto podría terminar su salario base")
        width: parent.width * 0.8
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 20
        wrapMode: Text.WordWrap
    }

    ColumnLayout
    {
        anchors.top: instrucciones.bottom
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width*0.8

        Label
        {
            text: qsTr("Salario en Bs.F:")
        }
        TextField
        {
            id: valor
            placeholderText: qsTr("Bs.F")
            inputMethodHints: Qt.ImhDigitsOnly
        }
        Button
        {
            text: qsTr("Calcular")
            onClicked:
            {
                var bsf = Calculos.calcularAumentoFuerte(valor.text);
                var bss = Calculos.conversionFuertesSoberanos(bsf);
                var ptr = Calculos.conversionBolivaresPetro(bss);

                resultadoBsF.text = qsTr("Su nuevo salario es de " + bsf + "Bs.F");
                resultadoBsS.text = qsTr("Su nuevo salario es de " + bss + "Bs.S");
                resultadoPtr.text = qsTr("Su nuevo salario es de " + ptr + "PTR");

                resultadoBsF.visible = true;
                resultadoBsS.visible = true;
                resultadoPtr.visible = true;
            }
        }
        Text
        {
            id: resultadoBsF
            visible: false
            Layout.preferredWidth: parent.width
            wrapMode: Text.WordWrap
        }
        Text
        {
            id: resultadoBsS
            visible: false
            Layout.preferredWidth: parent.width
            wrapMode: Text.WordWrap
        }
        Text
        {
            id: resultadoPtr
            visible: false
            Layout.preferredWidth: parent.width
            wrapMode: Text.WordWrap
        }

    }

}
