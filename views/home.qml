import QtQuick 2.9
import QtQuick.Controls 2.2

Page
{
    width: parent.width
    height: parent.height


    Image
    {
        id: logo
        source: "qrc:/recursos/logoSoberano"
        width: parent.width*0.5
        height: width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 20
    }

    Text
    {
        width: parent.width*0.8
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: logo.bottom
        anchors.topMargin: 10
        text: qsTr("Esta app busca ser una ayuda para facilitar el calculo de valores luego de la reconversión monetaria.
Es una aplicación informativa y debe usarse para ese fin.
Para iniciar oprima el botón de menú en la esquina superior izquierda y elija una opción.")
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
    }


}
