import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Page
{
    id: acerca

    title: qsTr("Acerca De")

    Image
    {
        id: logo
        source: "qrc:/recursos/logoDisIngLab"
        width: parent.width*0.5
        height: width
        anchors.horizontalCenter: parent.horizontalCenter
    }

    ColumnLayout
    {
        width: parent.width
        anchors.top: logo.bottom

        Text
        {
            text: qsTr("Calculador de Reconversión ver 0.5")
            font.pointSize: 14
            Layout.alignment: Qt.AlignHCenter
        }
        Text
        {
            text: qsTr("Licencia GPL3.")
            font.pointSize: 14
            Layout.alignment: Qt.AlignHCenter
        }
        Text
        {
            text: qsTr("desarrollado por")
            font.pointSize: 14
            Layout.alignment: Qt.AlignHCenter
        }
        Text
        {
            text: qsTr("DisIngLab Hardware y Software C.A.")
            font.pointSize: 14
            Layout.alignment: Qt.AlignHCenter
        }
        Text
        {
            text: qsTr("RIF: J-41074005-1")
            font.pointSize: 14
            Layout.alignment: Qt.AlignHCenter
        }
        Text
        {
            text: qsTr("Correo: disingproyecto@gmail.com")
            font.pointSize: 14
            Layout.alignment: Qt.AlignHCenter
        }

    }
}
