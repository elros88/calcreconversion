import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import "qrc:/utils/calculos.js" as Calculos

Page
{
    width: parent.width
    height: parent.height

    title: qsTr("Salario Dolar")

    Text
    {
        id: instrucciones;
        text: qsTr("Introduzca su ingreso en Dólares y obtenga su equivalente en Bolívares Soberanos y Petros")
        width: parent.width * 0.8
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 20
        wrapMode: Text.WordWrap
    }

    ColumnLayout
    {
        anchors.top: instrucciones.bottom
        anchors.topMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width*0.8

        Label
        {
            text: qsTr("Salario en USD:")
        }
        TextField
        {
            id: valor
            placeholderText: qsTr("USD")
            inputMethodHints: Qt.ImhDigitsOnly
        }
        Button
        {
            text: qsTr("Calcular")
            onClicked:
            {
                var bss = Calculos.calcularSalarioDolarBolivar(valor.text);
                var ptr = Calculos.calcularSalarioDolarPetro(valor.text);

                resultadoBsS.text = qsTr("Su salario es de " + bss + "Bs.S");
                resultadoPtr.text = qsTr("Su salario es de " + ptr + "PTR");

                resultadoBsS.visible = true;
                resultadoPtr.visible = true;
            }
        }

        Text
        {
            id: resultadoBsS
            visible: false
            Layout.preferredWidth: parent.width
            wrapMode: Text.WordWrap
        }
        Text
        {
            id: resultadoPtr
            visible: false
            Layout.preferredWidth: parent.width
            wrapMode: Text.WordWrap
        }

    }

}
