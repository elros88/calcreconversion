#ifndef CONSTANTES_H
#define CONSTANTES_H

#include <QObject>

class Constantes : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int bsAptr READ getBsAptr)
    Q_PROPERTY(int dolarAptr READ getDolarAptr)
    Q_PROPERTY(int porcentajeAumento READ getPorcentajeAumento)
    Q_PROPERTY(int reduccionCeros READ getReduccionCeros)

    int bsAptr;
    int dolarAptr;
    int porcentajeAumento;
    int reduccionCeros;

public:
    explicit Constantes(QObject *parent = nullptr);

    int getBsAptr() const;
    void setBsAptr(int value);

    int getDolarAptr() const;
    void setDolarAptr(int value);

    int getPorcentajeAumento() const;
    void setPorcentajeAumento(int value);

    int getReduccionCeros() const;
    void setReduccionCeros(int value);

signals:

public slots:
};

#endif // CONSTANTES_H
