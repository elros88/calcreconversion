#include "constantes.h"

int Constantes::getBsAptr() const
{
    return bsAptr;
}

void Constantes::setBsAptr(int value)
{
    bsAptr = value;
}

int Constantes::getDolarAptr() const
{
    return dolarAptr;
}

void Constantes::setDolarAptr(int value)
{
    dolarAptr = value;
}

int Constantes::getPorcentajeAumento() const
{
    return porcentajeAumento;
}

void Constantes::setPorcentajeAumento(int value)
{
    porcentajeAumento = value;
}

int Constantes::getReduccionCeros() const
{
    return reduccionCeros;
}

void Constantes::setReduccionCeros(int value)
{
    reduccionCeros = value;
}

Constantes::Constantes(QObject *parent) : QObject(parent)
{
    bsAptr = 3600;
    dolarAptr = 60;
    porcentajeAumento = 3600;
    reduccionCeros = 100000;
}
